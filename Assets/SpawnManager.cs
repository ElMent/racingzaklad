using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform ObjectToFollow;
    public GameObject[] Spawns;
    public GameObject[] Prefabs;
    public float timeRate = 40.0f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnSpider());
    }

    private IEnumerator SpawnSpider()
    {
        while (true)
        {
            var prefab = Prefabs[Random.Range(0, Prefabs.Length)];
            var spawn = Spawns[Random.Range(0, Spawns.Length)];
            GameObject spider = Instantiate(prefab, spawn.transform.position, Quaternion.Euler(0, 0, 0));
            spider.transform.SetParent(transform);
            var wanderingScript = spider.GetComponent<Wandering>();
            if (wanderingScript != null) wanderingScript.Destinations = Spawns;
            var followScript = spider.GetComponent<Follow>();
            if (followScript != null) followScript.ObjectToFollow = ObjectToFollow;
            yield return new WaitForSeconds(timeRate);
        }
    }
}
