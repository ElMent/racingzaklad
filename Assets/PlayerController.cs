using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    //privatni - mam je public abych je videl
    public float Turning;
    public float Acceleration;
    public float CurrentSpeed;
    public float WheelsAngle;

    //skutecne public - nastaveni z unity
    public float MaxSpeed;
    public float MaxBreak;
    public float MaxWheelsAngle;
    public float SlowDownCoef;
    public float AccelerationCoef;
    public float BreakingCoef;
    public float TurningCoef;
    public float TurningBackCoef;
    public List<AxleInfo> Axles;
    public int Points = 0;

    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        rb.centerOfMass = rb.centerOfMass - new Vector3(0, 0.2f, 0);
        rb.AddForce(new Vector3(0, -1.0f, 0)); 
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 vec = movementValue.Get<Vector2>();
        Turning = vec.x;
        Acceleration = vec.y;
    }

    private void FixedUpdate()
    {
        CurrentSpeed = MaxSpeed * Acceleration;
        WheelsAngle = MaxWheelsAngle * Turning;
        foreach (AxleInfo axleInfo in Axles)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = WheelsAngle;
                axleInfo.rightWheel.steerAngle = WheelsAngle;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = CurrentSpeed;
                axleInfo.rightWheel.motorTorque = CurrentSpeed;
                axleInfo.leftWheel.brakeTorque = CurrentSpeed==0?MaxBreak:0;
                axleInfo.rightWheel.brakeTorque = CurrentSpeed == 0 ? MaxBreak : 0;
                ApplyLocalPositionToVisuals(axleInfo.leftWheel, axleInfo.leftVisual);
                ApplyLocalPositionToVisuals(axleInfo.rightWheel, axleInfo.rightVisual);
            }
        }
    }

    public void ApplyLocalPositionToVisuals(WheelCollider collider, GameObject visual)
    {
        try
        {
            if (visual == null)
            {
                return;
            }
            Transform visualWheel = visual.transform;

            Vector3 position;
            Quaternion rotation;
            collider.GetWorldPose(out position, out rotation);

            visualWheel.transform.position = position;
            visualWheel.transform.rotation = rotation;
            visualWheel.transform.Rotate(Vector3.up, -90);
        }
        catch (Exception ex)
        {
            print(ex.ToString());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CollectEnemy"))
        {
            Points++;
            Destroy(collision.gameObject, 2);
        }

        if (collision.gameObject.CompareTag("KillerEnemy"))
        {
            Points = 0;
            Destroy(collision.gameObject, 2);
        }
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public GameObject leftVisual;
    public GameObject rightVisual;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
}
